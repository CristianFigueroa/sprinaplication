package com.example.controller;

import com.example.components.PostComponent;
import com.example.model.Post;
import com.example.pages.Pagina;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("")
public class ControllerBasic {
    Log log = LogFactory.getLog(getClass());


    @Autowired
    private PostComponent _postComponent;

    @GetMapping(path = {"/home", "/"})
    public String mostrarPosts(Model model) {
        model.addAttribute("posts", this._postComponent.getPost());
        return Pagina.HOME;
    }

// Lo mismo de saludo() pero codificado de otra forma
//  @GetMapping(path = {"/post"})
//  public ModelAndView post(Model model) {
//      ModelAndView modelAndView = new ModelAndView(Pagina.HOME);
//      modelAndView.addObject("posts", this.getPost());_postComponent
//      return modelAndView;
//  }

    @GetMapping(path = {"/post"})
    public ModelAndView vistaPost(
            //@requestParam para variables de post?id=2
            //@PathVariable para variables de post/2
            @RequestParam(name = "id", required = true) int id
            ) { //parametro por url
        return vistaPostId(id);
    }

    //POST FILTRADO con variable de ingresada de otra forma
    @GetMapping(path = {"/post/{id}"})
    public ModelAndView vistaPost_otraRuta(
            //@RequestParam para variables de post?id=2
            //@PathVariable para variables de post/2
            @PathVariable(required = true, name = "id")  int id
    ) { //parametro por url
        return vistaPostId(id);
    }

    private ModelAndView vistaPostId(int id){
        ModelAndView modelAndView = new ModelAndView(Pagina.POST);
        List<Post> postFiltrado =this._postComponent.getPost().stream()
                .filter((post -> {
                    return post.getId() == id;
                })).collect(Collectors.toList());
        modelAndView.addObject("post", postFiltrado.get(0)); //le pasamos el primer post encontrado que deberia ser el unico
        return modelAndView;
    }

    //----------------------------------------------------------------------------

    @GetMapping(path = {"/nuevoPost","/nuevopost"})
    public ModelAndView vistaFormulario(){
        return new ModelAndView(Pagina.FORMULARIO_NUEVO_POST).addObject("post", new Post());
    }

    @PostMapping("/añadirPost")
    public String añadirNuevoPost(Post post, Model model){ // post es el objeto que recibo desde el formulario
        List<Post> posts = this._postComponent.getPost();
        posts.add(post);
        model.addAttribute("posts",posts);
        return Pagina.HOME;
    }
}
