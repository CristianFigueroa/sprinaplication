package com.example.services.implementation;

import com.example.model.Post;
import com.example.services.PostService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {
    Log log = LogFactory.getLog(getClass());

    @Override
    public List<Post> validarId(List<Post> posts) {
        for(Post post: posts){
            if(post.getTitulo()==null){
                log.error("No tiene titulo");
                throw new NullPointerException();
            }
        }
        return posts;
    }
}
