package com.example.services;

import com.example.model.Post;

import java.util.List;

public interface PostService {
    public List<Post> validarId(List<Post> posts);
}
