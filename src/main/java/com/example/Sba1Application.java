package com.example;

import com.example.components.PostComponent;
import com.example.model.Conexion;
import com.example.services.PostService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sba1Application implements CommandLineRunner {
	Log log = LogFactory.getLog(getClass());

	@Autowired
	@Qualifier("beanConexion") //nombre del bean de beans.CrearConexion
	private Conexion conexion;

	@Autowired
	@Qualifier("postComponent")
	public PostComponent postComponent;

	@Autowired
	public PostService postService;

	public static void main(String[] args) {
		SpringApplication.run(Sba1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception{
		postService.validarId(postComponent.getPost()).forEach(post -> {
			log.info(post.getTitulo());
		});
	}
}
