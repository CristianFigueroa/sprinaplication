package com.example.components;

import com.example.model.Post;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("postComponent")
public class PostComponent {
    Log log = LogFactory.getLog(getClass());

    public List<Post> getPost() {
        ArrayList<Post> post = new ArrayList<>();
        post.add(new Post(1,
                "http://localhost:8080/img/card.jpg",
                "Desarrollo web 111111",
                "Some quick example text to build on the card title and make up the bulk of the card's content.",
                new Date()));
        post.add(new Post(2,
                "http://localhost:8080/img/card.jpg",
                "Desarrollo web 22222",
                "Some quick example text to build on the card title and make up the bulk of the card's content.",
                new Date()));
        post.add(new Post(3,
                "http://localhost:8080/img/card.jpg",
                "Desarrollo web 333333",
                "Some quick example text to build on the card title and make up the bulk of the card's content.",
                new Date()));
        post.add(new Post(4,
                "http://localhost:8080/img/card.jpg",
                "Desarrollo web 444444",
                "Some quick example text to build on the card title and make up the bulk of the card's content.",
                new Date()));
        return post;
    }

}
