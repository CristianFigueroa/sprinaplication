package com.example.model;

import java.util.Date;

public class Post {

    private int id;
    private String urlImg = "http://localhost:8080/img/card.jpg";
    private String titulo;
    private String texto;
    private Date fecha = new Date();

    public Post(int id, String urlImg, String titulo, String texto, Date fecha) {
        this.id = id;
        this.urlImg = urlImg;
        this.titulo = titulo;
        this.texto = texto;
        //this.fecha = fecha;
    }

    public Post() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
