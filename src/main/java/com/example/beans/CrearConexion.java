package com.example.beans;

import com.example.model.Conexion;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CrearConexion {
    @Bean(name = "beanConexion")
    public Conexion getConexion(){
        Conexion c = new Conexion();
        c.setDb("mysql");
        c.setUrl("localhost");
        return c;
    }
}
